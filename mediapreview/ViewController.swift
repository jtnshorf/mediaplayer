//
//  ViewController.swift
//  mediapreview
//
//  Created by Shrofile on 28/06/17.
//  Copyright © 2017 Shrofile. All rights reserved.
//

import UIKit
import AVFoundation

private var playerViewControllerKVOContext = 0
class ViewController: UIViewController {
    
    
    @IBOutlet weak var playPauseBtn: UIButton!
    @IBOutlet weak var playerView: PlayerView!
    @IBOutlet weak var scrubber: UISlider!
    @IBOutlet weak var previewCollection: UICollectionView!
    @IBOutlet weak var progresLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    let player = AVPlayer()
    var timeObserverToken: Any?
    var itemIndex = 0{
        didSet{
            if let asset = inputSegments[itemIndex].avAsset{
                let playerItem = AVPlayerItem(asset: asset)
                player.replaceCurrentItem(with: playerItem)
                //highlighting corresponding preview
                highlightCell(atIndex: itemIndex)
                if player.rate != 1 && itemIndex != 0{
                    player.play()
            }}
        }}
    var totalDuration: Float = 0{
        didSet{
            scrubber.maximumValue = totalDuration
            durationLabel.text = createTimeString(time: totalDuration)
            
        }
    }
    var isQueueMode = true{
        didSet{
            scrubber.isUserInteractionEnabled = !isQueueMode
        }
    }
    var elapsedDuration: Float = 0
    var assets:[String] = ["sample","sample1","sample2"]
    var inputSegments = [AssetSegment]()
    var outputSegments = [AssetSegment]()
    var scrubberValue: Float = 0{
        didSet{
            scrubber.value = scrubberValue
        }
    }
    let timeRemainingFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.zeroFormattingBehavior = .pad
        formatter.allowedUnits = [.minute, .second]
        
        return formatter
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playerView.playerLayer.player = player
        
        for asset in assets{
            guard let assetUrl = Bundle.main.path(forResource: asset, ofType: "mp4")
                else {return}
            let avAsset = AVURLAsset(url: URL(fileURLWithPath: assetUrl))
            let secondsDuration = Float(CMTimeGetSeconds(avAsset.duration))
            let thumbnail = getSegmentThumbnail(asset: avAsset)
            self.inputSegments.append(AssetSegment(asset: avAsset, thumb: thumbnail, duration: secondsDuration))
            totalDuration += secondsDuration
        }
        
        //adding first item to avplayer
        let playerItem = AVPlayerItem(asset: inputSegments[0].avAsset!)
        player.replaceCurrentItem(with: playerItem)
        //generating thumbnails
        
        //reload collection view to show segment thumbs
        previewCollection.reloadData()
        
        //adding time observer on player
        let interval = CMTimeMake(1, 1)
        timeObserverToken = player.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main) { time in
            let timeInSeconds = Float(CMTimeGetSeconds(time))
            var offset: Float = 0
            //calculating offset
            for (index,item) in self.inputSegments.enumerated(){
                if index == self.itemIndex{
                    break
                }
                offset += item.duration!
            }
            
            self.scrubberValue = offset + timeInSeconds
            self.progresLabel.text = self.createTimeString(time: self.scrubberValue)
            print("ScrubberValue: \(self.scrubberValue); totalValue: \(self.totalDuration) ; index: \(self.itemIndex)")
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        addObserver(self, forKeyPath: #keyPath(ViewController.player.currentItem.duration), options: [.new, .initial], context: &playerViewControllerKVOContext)
        addObserver(self, forKeyPath: #keyPath(ViewController.player.rate), options: [.new, .initial], context: &playerViewControllerKVOContext)
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemEndPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    func playerItemEndPlaying(notification: NSNotification){
        //finished playing one item
        //find if next item exists and add it to the player
        if inputSegments.indices.contains(itemIndex+1){
            itemIndex += 1
        }else{
            itemIndex = 0
        }
    }
    
    func configurePlayitem(atIndex Index: Int){
        
    }
    
    @IBAction func playButtonTapped(_ sender: Any) {
        if player.rate != 1{
            //not playing, play
            player.play()
        }else{
            player.pause()
        }
        
    }
    
    
    @IBAction func sliderChanged(_ sender: UISlider) {
        let currentTime = (sender).value
        var seekDuration: Float = 0.0
        var potentialCurrentItemIndex: Int?
        var transformedSeek: Int = 0
        //translating
        for (index,segment) in inputSegments.enumerated(){
            if let duration = segment.duration{
                seekDuration += duration
                if seekDuration >= currentTime{
                    potentialCurrentItemIndex = index
                    break
                }
            }
        }
        
        //converting slider value to current time of potential item
        if potentialCurrentItemIndex != nil{
            let asset = inputSegments[potentialCurrentItemIndex!].avAsset!
            let playerItem = AVPlayerItem(asset: asset)
            player.replaceCurrentItem(with: playerItem)
            self.itemIndex = potentialCurrentItemIndex!
            transformedSeek = Int(currentTime - seekDuration + inputSegments[potentialCurrentItemIndex!].duration!)
        }
        print("Current Time: \(currentTime) ; Index: \(potentialCurrentItemIndex) ; Transformed : \(transformedSeek)")
        player.seek(to: CMTimeMakeWithSeconds(Float64(transformedSeek), 1), toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
    }
    
    //KVO Observer
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &playerViewControllerKVOContext{
            if keyPath == #keyPath(ViewController.player.currentItem.duration){
                var duration: CMTime
                if let durationValue = change?[.newKey] as? NSValue{
                    duration = durationValue.timeValue
                }else{
                    duration = kCMTimeZero
                }
                let hasValidDuration = duration.isNumeric && duration.value != 0
                if hasValidDuration{

                }
                
            }else if keyPath == #keyPath(ViewController.player.rate){
                guard let newRate = change?[.newKey] as? Double else {return}
                let btnTitle = (newRate == 1.0) ? "Pause" : "Play"
                playPauseBtn.setTitle(btnTitle, for: .normal)
            }else if keyPath == "duration"{
                guard let duration = change?[.newKey] as? NSValue else {return}
                let secondsDuration = CMTimeGetSeconds(duration.timeValue)
                totalDuration += Float(secondsDuration)
            }
            
            
            
        }else{
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    
    func getSegmentThumbnail(asset: AVAsset) -> UIImage?{
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        guard let cgImage = try? imageGenerator.copyCGImage(at: CMTimeMake(5, 1), actualTime: nil) else{
            print("error generating thumbnail image")
            return nil
        }
        
        let thumb = UIImage(cgImage: cgImage)
        return thumb
    }
    
    func highlightCell(atIndex index: Int){
        //unhighlighting cells
        for cell in previewCollection.visibleCells{
            let overlayView = cell.viewWithTag(2)
            overlayView?.backgroundColor = .clear
        }

        let indexPath = IndexPath(item: index, section: 0)
        let cell = previewCollection.cellForItem(at: indexPath)
        let overlayView = cell?.viewWithTag(2)!
        overlayView?.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        //jumping to the highlighted cell
        previewCollection.scrollToItem(at: indexPath, at: .left, animated: true)
    }
    
    func createTimeString(time: Float) -> String {
        let components = NSDateComponents()
        components.second = Int(max(0.0, time))
        
        return timeRemainingFormatter.string(from: components as DateComponents)!
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return inputSegments.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let imageView = cell.viewWithTag(1) as! UIImageView
        if let image = inputSegments[indexPath.item].thumbnail{
            imageView.image = image
        }else{
            
        }
        let overlayView = cell.viewWithTag(2)!
        overlayView.backgroundColor = .clear
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //set itemIndex to selected cell indexpath
        self.itemIndex = indexPath.item
    }
}

extension AVQueuePlayer{
    public func indexOf(playerItem: AVPlayerItem) -> Int{
        var index = 0
        for item in items(){
            if item == playerItem{
                return index
            }
            index += 1
        }
        return -1
    }
}
