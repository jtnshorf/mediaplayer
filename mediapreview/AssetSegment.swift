//
//  AssetSegment.swift
//  mediapreview
//
//  Created by Shrofile on 28/06/17.
//  Copyright © 2017 Shrofile. All rights reserved.
//

import AVFoundation
import UIKit

class AssetSegment: NSObject{
    var avAsset: AVURLAsset?
    var thumbnail: UIImage?
    var duration: Float?
    
    override init() {
       super.init()
    }
    
    init(asset: AVURLAsset?, thumb: UIImage?, duration: Float) {
        super.init()
        self.avAsset = asset
        self.thumbnail = thumb
        self.duration = duration
    }
    
    
}
